import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class assigment {
    private JButton karaageButton;
    private JButton TakoyakiButton;
    private JButton YakitoriButton;
    private JButton Yakisobabutton;
    private JButton RamenButton;
    private JButton GyozaButton;
    private JTextField textField1;
    private JButton checkOutButton;
    private JTextPane textPane2;
    private JLabel goukei;
    private JPanel root;
    int max=0;
    public static void main(String[] args) {
        JFrame frame = new JFrame("assigment");
        frame.setContentPane(new assigment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order (String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confilmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            String t = textPane2.getText();
            textPane2.setText(t+"\n"+food);
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"!");
            int price=100;
            max+=price;
            goukei.setText(max+"yen");
        }
    }
    public assigment() {
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        Yakisobabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        TakoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki");
            }
        });
        YakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        RamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int a = JOptionPane.showConfirmDialog(null,
                        "Would you like to Checkout?",
                        "Checkout Confilmation",
                        JOptionPane.YES_NO_OPTION);
                if(a==0){
                    JOptionPane.showMessageDialog(null,"Thank you! Total price is "+max+" yen.");
                    textPane2.setText("");
                    max=0;
                    goukei.setText("0yen");
                }
            }
        });
    }
}
